#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(
        curandState *devState, size_t seed, float *colors, size_t bw, size_t bh, size_t c_arr_size
) {
    curand_init(seed, 0, 0, &*devState);
    for (size_t c = 0; c < c_arr_size; c++) {
        colors[c] = curand_uniform(&*devState) * 128.0f + 127.0f;
    }
}
__global__ void create(curandState *devState, unsigned char *png_buf,
        size_t w, size_t h, size_t bw, size_t bh, float *colors, size_t bxn, size_t byn, size_t bl, size_t bt, size_t p
) {
    size_t bx = blockIdx.x % bw;
    size_t by = blockIdx.x / bw;
    for (size_t y = bt + (by * byn) + p; y < bt + (by * byn) + byn - p; y++) {
        for (size_t x = bl + (bx * bxn) + p; x < bl + (bx * bxn) + bxn - p; x++) {
            png_buf[(((y * w) + x) * 3) + 0] = colors[(((by * bw) + bx) * 3) + 0];
            png_buf[(((y * w) + x) * 3) + 1] = colors[(((by * bw) + bx) * 3) + 1];
            png_buf[(((y * w) + x) * 3) + 2] = colors[(((by * bw) + bx) * 3) + 3];
        }
    }
}

int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920*2;
    size_t h = 1080*2;
    size_t bw = 20;
    size_t bh = 15;
    size_t c_arr_size = bw * bh * 3;
    size_t bxn = w / bw;
    size_t byn = h / bh;
    size_t bl = (w - (bxn * bw)) / 2;
    size_t bt = (h - (byn * bh)) / 2;
    size_t p = 15;

    curandState *devState;
    unsigned char *png_buf;
    float *colors;

    cudaMalloc(&devState, sizeof(curandState));
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);
    cudaMalloc(&colors, sizeof(float) * c_arr_size);

    setup <<< 1, 1 >>> (devState, time(0), colors, bw, bh, c_arr_size);
    create <<< bw * bh, 1 >>> (devState, png_buf, w, h, bw, bh, colors, bxn, byn, bl, bt, p);


    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "blocks");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
    cudaFree(colors);
}
