#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(
        curandState *devState, size_t seed, float *color,
        size_t number_of_colors, float *centroids, size_t number_of_centroids, float *color_centroids,
        size_t number_of_color_centroids, float centroid_max_x, float centroid_max_y
) {
    curand_init(seed, 0, 0, &*devState);
    for (size_t i = 0; i < number_of_colors; i++) {
        color[(i * 3) + 0] = curand_uniform(&*devState) * 255.0f;
        color[(i * 3) + 1] = curand_uniform(&*devState) * 255.0f;
        color[(i * 3) + 2] = curand_uniform(&*devState) * 255.0f;
    }
    for (size_t i = 0; i < number_of_centroids; i++) {
        centroids[(i * 2) + 0] = curand_uniform(&*devState) * centroid_max_x;
        centroids[(i * 2) + 1] = curand_uniform(&*devState) * centroid_max_y;
    }
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        color_centroids[(i * 2) + 0] = curand_uniform(&*devState) * centroid_max_x;
        color_centroids[(i * 2) + 1] = curand_uniform(&*devState) * centroid_max_y;
    }
}

__global__ void calc_centroid_colors(
        float *centroid_distances, float *centroids, float *color_centroids, size_t number_of_color_centroids,
        float *centroid_colors, float *color, float max_dist
) {
    size_t centroid_index = blockIdx.x;
    float total_dist = 0;
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        centroid_distances[(centroid_index*number_of_color_centroids) + i] = sqrtf(
                powf(centroids[(centroid_index*2)+0] - color_centroids[(i * 2) + 0], 2.0f) +
                powf(centroids[(centroid_index*2)+1] - color_centroids[(i * 2) + 1], 2.0f)
        );
        total_dist += centroid_distances[(centroid_index*number_of_color_centroids) + i];
    }
    for (size_t c = 0; c < 3; c++) {
        centroid_colors[(centroid_index * 3) + c] = 0;
    }
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        for (size_t c = 0; c < 3; c++) {
            centroid_colors[(centroid_index * 3) + c] +=
                color[(i * 3) + c] * (
                    1.0f - fminf(centroid_distances[(centroid_index*number_of_color_centroids) + i] / max_dist, 1.0f)
                );
        }
    }
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        for (size_t c = 0; c < 3; c++) {
            centroid_colors[(centroid_index * 3) + c] = fminf(centroid_colors[(centroid_index * 3) + c], 255.0f);
        }
    }
}

__global__ void calc_pixel_colors(
        unsigned char * png_buf, float * centroids, float * centroid_colors, size_t number_of_centroids, size_t width
) {
    size_t x = blockIdx.x % width;
    size_t y = blockIdx.x / width;

    float min_dist = -1;
    size_t min_dist_index = 0;

    for (size_t i = 0; i < number_of_centroids; i++) {
        float distance = sqrtf(
                powf(x - centroids[(i * 2) + 0], 2.0f) +
                powf(y - centroids[(i * 2) + 1], 2.0f)
        );
        if (distance < min_dist || min_dist == -1) {
            min_dist = distance;
            min_dist_index = i;
        }
    }

    png_buf[(y*width*3)+(x*3)+0] = centroid_colors[(min_dist_index*3)+0];
    png_buf[(y*width*3)+(x*3)+1] = centroid_colors[(min_dist_index*3)+1];
    png_buf[(y*width*3)+(x*3)+2] = centroid_colors[(min_dist_index*3)+2];
}

int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920;
    size_t h = 1080;
    size_t color_factor = 150;
    size_t vor_factor = 30;

    size_t number_of_colors = (w / color_factor) * (h / color_factor);
    size_t color_channels = 3;
    size_t color_vector_size = number_of_colors * color_channels;
    size_t number_of_centroids = (w / vor_factor) * (h / vor_factor);
    size_t centroid_vector_size = number_of_centroids * 2;
    size_t number_of_color_centroids = (w / color_factor) * (h / color_factor);
    size_t color_centroid_vector_size = number_of_color_centroids * 2;

    curandState *devState;
    float *color;
    float *centroids;
    float *color_centroids;
    float *centroid_distances;
    float *centroid_colors;
    unsigned char *png_buf;

    float max_dist = 200.0f;

    cudaMalloc(&devState, sizeof(curandState));
    cudaMalloc(&color, sizeof(float) * color_vector_size);
    cudaMalloc(&centroids, sizeof(float) * centroid_vector_size);
    cudaMalloc(&color_centroids, sizeof(float) * color_centroid_vector_size);
    cudaMalloc(&centroid_distances, sizeof(float) * number_of_centroids * number_of_color_centroids);
    cudaMalloc(&centroid_colors, sizeof(float) * number_of_centroids * color_channels);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (
            devState, time(0), color, number_of_colors, centroids, number_of_centroids,
                    color_centroids, number_of_color_centroids, w, h
    );

    calc_centroid_colors <<< number_of_centroids, 1 >>> (
            centroid_distances, centroids, color_centroids, number_of_color_centroids,
            centroid_colors, color, max_dist
    );

    calc_pixel_colors <<< h * w, 1 >>> (
            png_buf, centroids, centroid_colors, number_of_centroids, w
    );

    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "voronoi");
    free(buffer);

    cudaFree(devState);
    cudaFree(color);
    cudaFree(centroids);
    cudaFree(color_centroids);
    cudaFree(centroid_distances);
    cudaFree(centroid_colors);
    cudaFree(png_buf);
}
