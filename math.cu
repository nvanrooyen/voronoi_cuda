#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(
        curandState *devState, size_t seed
) {
    curand_init(seed, 0, 0, &*devState);
}

__global__ void calc_pixel_colors(
        unsigned char *png_buf, size_t width
) {
    size_t x = blockIdx.x % width;
    size_t y = blockIdx.x / width;
    float fx = x;
    float fy = y;
    float factor = width / 7;
    float bf = fmaxf((sinf(fx / 100)/4)+0.75, 0);
    png_buf[(y * width * 3) + (x * 3) + 0] = (sinf(cosf(fx / factor) + cosf(fy / factor)) * 80.0f * bf) + 80 * bf;
    png_buf[(y * width * 3) + (x * 3) + 1] = (sinf(cosf(fx / factor) - cosf(fy / factor)) * 80.0f * bf) + 80 * bf;
    png_buf[(y * width * 3) + (x * 3) + 2] = (cosf(sinf(fx / factor) + sinf(fy / factor)) * 80.0f * bf) + 80 * bf;
}

int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920;
    size_t h = 1080;

    size_t color_channels = 3;

    curandState *devState;
    unsigned char *png_buf;

    cudaMalloc(&devState, sizeof(curandState));
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (devState, time(0));

    calc_pixel_colors << < h * w, 1 >> > (png_buf, w);

    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "voronoi");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
}
