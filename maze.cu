#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(curandState *devState, size_t random_dev_count, size_t seed, unsigned char *png_buf) {
  curand_init(seed, 0, 0, &devState[0]);
  for (size_t i = 1; i < random_dev_count; i++) {
    curand_init(curand_uniform(&*devState) * seed, 0, 0, &devState[i]);
  }
}


__device__ void white(unsigned char *png_buf, size_t x, size_t y, size_t w) {
  for (int c = 0; c < 3; c++) {
    png_buf[(((y * w) + x) * 3) + c] = 255;
  }
}


__device__ double prob (double xd, double yd)
{
  if (xd < 20 || yd < 20) {
    if (xd < yd) {
      return (xd / 40);
    }
    return 1 - (yd / 40);
  }
  return 0.5;
}

__global__ void gen(curandState *devState, unsigned char *png_buf, size_t w, size_t h) {
  size_t x = ((blockIdx.x % (w / 2)) * 2) + 1;
  size_t y = ((blockIdx.x / (w / 2)) * 2) + 1;
  white(png_buf, x, y, w);
  if (x < w - 2) {
    if (y < h - 2) {
      if (curand_uniform(&devState[((y * w) + x)]) < prob(w - x, h - y)) {
        white(png_buf, x + 1, y, w);
      } else {
        white(png_buf, x, y + 1, w);
      }
    } else {
      white(png_buf, x + 1, y, w);
    }
  } else {
    if (y < h - 2) {
      white(png_buf, x, y + 1, w);
    }
  }
}


int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}


int main() {

    size_t w = (1920*2)+1;
    size_t h = (1080*2)+1;
    size_t random_dev_count = w*h;

    curandState *devState;
    unsigned char *png_buf;
    unsigned char *png_buf2;

    cudaMalloc(&devState, sizeof(curandState) * random_dev_count);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (devState, random_dev_count, time(0), png_buf);

    gen <<< (w / 2) * (h / 2), 1 >>> (devState, png_buf, w, h);

    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    buffer[3] = 255;
    buffer[(w * h * 3) - 5] = 255;
    writeImage("out.png", w, h, buffer, "blocks");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
    cudaFree(png_buf2);
}
