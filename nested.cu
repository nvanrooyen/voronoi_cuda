#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(
        curandState *devState, size_t seed, float *color, float *color_dist, float r, float g, float b,
        size_t number_of_colors, float *centroids, size_t number_of_centroids, float *color_centroids,
        size_t number_of_color_centroids, float centroid_max_x, float centroid_max_y
) {
    curand_init(seed, 0, 0, &*devState);
    for (size_t i = 0; i < number_of_colors; i++) {
        color[(i * 3) + 0] = curand_uniform(&*devState) * 255.0f;
        color[(i * 3) + 1] = curand_uniform(&*devState) * 255.0f;
        color[(i * 3) + 2] = curand_uniform(&*devState) * 255.0f;

        color_dist[(i * 3) + 0] = r - color[(i * 3) + 0];
        color_dist[(i * 3) + 1] = g - color[(i * 3) + 1];
        color_dist[(i * 3) + 2] = b - color[(i * 3) + 2];
    }
    for (size_t i = 0; i < number_of_centroids; i++) {
        centroids[(i * 2) + 0] = curand_uniform(&*devState) * centroid_max_x;
        centroids[(i * 2) + 1] = curand_uniform(&*devState) * centroid_max_y;
    }
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        color_centroids[(i * 2) + 0] = curand_uniform(&*devState) * centroid_max_x;
        color_centroids[(i * 2) + 1] = curand_uniform(&*devState) * centroid_max_y;
    }
}

__global__ void calc_centroid_color_indexes(
        size_t *centroid_color_indexes, float *centroids, float *color_centroids, size_t number_of_color_centroids
) {
    size_t centroid_index = blockIdx.x;
    float min_dist = -1;
    size_t min_dist_index = 0;
    for (size_t i = 0; i < number_of_color_centroids; i++) {
        float distance = sqrtf(
                powf(centroids[(centroid_index*2)+0] - color_centroids[(i * 2) + 0], 2.0f) +
                powf(centroids[(centroid_index*2)+1] - color_centroids[(i * 2) + 1], 2.0f)
        );
        if (distance < min_dist || min_dist == -1) {
            min_dist = distance;
            min_dist_index = i;
        }
    }
    centroid_color_indexes[centroid_index] = min_dist_index;
}

__global__ void calc_centroid_colors(
        float *centroid_colors, size_t *centroid_color_indexes, float *centroids, float *color, float *color_dist,
        float *color_centroids, float max_dist
) {
    size_t centroid_index = blockIdx.x;
    for (int c = 0; c < 3; c++) {
        centroid_colors[(centroid_index * 3) + c] =
                color[(centroid_color_indexes[centroid_index] * 3) + c] +
                color_dist[(centroid_color_indexes[centroid_index] * 3) + c] *
                (fminf(sqrtf(
                        powf(
                                color_centroids[(centroid_color_indexes[centroid_index] * 2) + 0] -
                                centroids[(centroid_index * 2) + 0],
                                2.0f
                        ) + powf(
                                color_centroids[(centroid_color_indexes[centroid_index] * 2) + 1] -
                                centroids[(centroid_index * 2) + 1],
                                2.0f
                        )
                ), max_dist) / max_dist);
    }
}

__global__ void calc_pixel_colors(
        unsigned char * png_buf, float * centroids, float * centroid_colors, size_t number_of_centroids, size_t width
) {
    size_t x = blockIdx.x % width;
    size_t y = blockIdx.x / width;

    float min_dist = -1;
    size_t min_dist_index = 0;

    for (size_t i = 0; i < number_of_centroids; i++) {
        float distance = sqrtf(
                powf(x - centroids[(i * 2) + 0], 2.0f) +
                powf(y - centroids[(i * 2) + 1], 2.0f)
        );
        if (distance < min_dist || min_dist == -1) {
            min_dist = distance;
            min_dist_index = i;
        }
    }

    png_buf[(y*width*3)+(x*3)+0] = centroid_colors[(min_dist_index*3)+0];
    png_buf[(y*width*3)+(x*3)+1] = centroid_colors[(min_dist_index*3)+1];
    png_buf[(y*width*3)+(x*3)+2] = centroid_colors[(min_dist_index*3)+2];
}

int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920;
    size_t h = 1080;
    size_t color_factor = 300;
    size_t vor_factor = 30;

    size_t number_of_colors = (w / color_factor) * (h / color_factor);
    size_t color_channels = 3;
    size_t color_vector_size = number_of_colors * color_channels;
    size_t number_of_centroids = (w / vor_factor) * (h / vor_factor);
    size_t centroid_vector_size = number_of_centroids * 2;
    size_t number_of_color_centroids = (w / color_factor) * (h / color_factor);
    size_t color_centroid_vector_size = number_of_color_centroids * 2;

    curandState *devState;
    float *color;
    float *color_dist;
    float *centroids;
    float *color_centroids;
    size_t *centroid_color_indexes;
    float *centroid_colors;
    unsigned char *png_buf;

    float max_dist = w;
    if (h > max_dist) max_dist = h;
    max_dist /= 5.0f;

    cudaMalloc(&devState, sizeof(curandState));
    cudaMalloc(&color, sizeof(float) * color_vector_size);
    cudaMalloc(&color_dist, sizeof(float) * color_vector_size);
    cudaMalloc(&centroids, sizeof(float) * centroid_vector_size);
    cudaMalloc(&color_centroids, sizeof(float) * color_centroid_vector_size);
    cudaMalloc(&centroid_color_indexes, sizeof(size_t) * number_of_centroids);
    cudaMalloc(&centroid_colors, sizeof(float) * number_of_centroids * color_channels);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (
            devState, time(0), color, color_dist, 0, 0, 0, number_of_colors, centroids, number_of_centroids,
                    color_centroids, number_of_color_centroids, w, h
    );

    calc_centroid_color_indexes <<< number_of_centroids, 1 >>> (
            centroid_color_indexes, centroids, color_centroids, number_of_color_centroids
    );

    calc_centroid_colors <<< number_of_centroids, 1 >>> (
            centroid_colors, centroid_color_indexes, centroids, color, color_dist, color_centroids, max_dist
    );

    calc_pixel_colors <<< h * w, 1 >>> (
            png_buf, centroids, centroid_colors, number_of_centroids, w
    );

    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "voronoi");
    free(buffer);

    cudaFree(devState);
    cudaFree(color);
    cudaFree(color_dist);
    cudaFree(centroids);
    cudaFree(color_centroids);
    cudaFree(centroid_color_indexes);
    cudaFree(centroid_colors);
    cudaFree(png_buf);
}
