#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(curandState *devState, size_t max_diag, size_t seed, unsigned char *png_buf) {
  curand_init(seed, 0, 0, &devState[0]);
  for (size_t i = 1; i < max_diag; i++) {
    curand_init(curand_uniform(&*devState) * seed, 0, 0, &devState[i]);
  }

  int total = 510;
  png_buf[0] = curand_uniform(&*devState) * 255;
  total -= png_buf[0];
  png_buf[1] = curand_uniform(&*devState) * 255;
  total -= png_buf[1];

  while (total > 255) {
      // Cause curand_uniform produces values in the range (0,1]
      int r = (1 - curand_uniform(&*devState)) * 3;
      unsigned char tmp = curand_uniform(&*devState) * (255 - png_buf[r]);
      png_buf[r] += tmp;
      total -= tmp;
  }
  png_buf[2] = total;

}



__device__ void set_color(curandState *devState, unsigned char *png_buf, size_t x, size_t y, size_t w, size_t r) {
  if (x > 0 && y > 0) {
    // if (curand_uniform(&devState[r]) < 0.0001) {
    //   png_buf[(((y * w) + x) * 3) + 0] = curand_uniform(&devState[r]) * 255;
    //   png_buf[(((y * w) + x) * 3) + 1] = curand_uniform(&devState[r]) * 255;
    //   png_buf[(((y * w) + x) * 3) + 2] = curand_uniform(&devState[r]) * 255;
    // } else {
      png_buf[(((y * w) + x) * 3) + 0] = (png_buf[(((y * w) + x - 1) * 3) + 0] + png_buf[((((y - 1) * w) + x) * 3) + 0]) / 2;
      png_buf[(((y * w) + x) * 3) + 1] = (png_buf[(((y * w) + x - 1) * 3) + 1] + png_buf[((((y - 1) * w) + x) * 3) + 1]) / 2;
      png_buf[(((y * w) + x) * 3) + 2] = (png_buf[(((y * w) + x - 1) * 3) + 2] + png_buf[((((y - 1) * w) + x) * 3) + 2]) / 2;
    // }
    while (png_buf[(((y * w) + x) * 3) + 0] + png_buf[(((y * w) + x) * 3) + 1] + png_buf[(((y * w) + x) * 3) + 2] < 510) {
      int i = (1 - curand_uniform(&devState[r])) * 3;
      if (png_buf[(((y * w) + x) * 3) + i] < 255) png_buf[(((y * w) + x) * 3) + i] += 1;
    }
  } else {
    if (x > 0) {
      png_buf[(((y * w) + x) * 3) + 0] = png_buf[(((y * w) + x - 1) * 3) + 0];
      png_buf[(((y * w) + x) * 3) + 1] = png_buf[(((y * w) + x - 1) * 3) + 1];
      png_buf[(((y * w) + x) * 3) + 2] = png_buf[(((y * w) + x - 1) * 3) + 2];
    } else {
      png_buf[(((y * w) + x) * 3) + 0] = png_buf[((((y - 1) * w) + x) * 3) + 0];
      png_buf[(((y * w) + x) * 3) + 1] = png_buf[((((y - 1) * w) + x) * 3) + 1];
      png_buf[(((y * w) + x) * 3) + 2] = png_buf[((((y - 1) * w) + x) * 3) + 2];
    }
  }
  int give = (1 - curand_uniform(&devState[r])) * 3;
  while (png_buf[(((y * w) + x) * 3) + give] == 255) {
    give = (1 - curand_uniform(&devState[r])) * 3;
  }
  int take = (1 - curand_uniform(&devState[r])) * 3;
  while (take == give || png_buf[(((y * w) + x) * 3) + take] == 0) {
    take = (1 - curand_uniform(&devState[r])) * 3;
  }
  // unsigned char adjustment = (1 - curand_uniform(&devState[r])) * min(min(png_buf[(((y * w) + x) * 3) + give], 255 - png_buf[(((y * w) + x) * 3) + take]), 2);
  unsigned char adjustment = 1;
  png_buf[(((y * w) + x) * 3) + take] -= adjustment;
  png_buf[(((y * w) + x) * 3) + give] += adjustment;
}


__global__ void set_pixel_by_xr(curandState *devState, unsigned char *png_buf, size_t w, size_t xr) {
    size_t x = xr - blockIdx.x;
    size_t y = blockIdx.x;
    set_color(devState, png_buf, x, y, w, blockIdx.x);
}

__global__ void set_pixel_by_yr(curandState *devState, unsigned char *png_buf, size_t w, size_t yr) {
    size_t x = (w - 1) - blockIdx.x;
    size_t y = yr + blockIdx.x;
    set_color(devState, png_buf, x, y, w, blockIdx.x);
}


int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920*2;
    size_t h = 1080*2;
    size_t max_diag = w;

    curandState *devState;
    unsigned char *png_buf;

    cudaMalloc(&devState, sizeof(curandState) * max_diag);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (devState, max_diag, time(0), png_buf);

    for (size_t xr = 1; xr < w; xr++) {
      size_t thread_count = xr + 1;
      if (thread_count > h) {
        thread_count = h;
      }
      set_pixel_by_xr <<< thread_count, 1 >>> (devState, png_buf, w, xr);
    }

    for (size_t yr = 1; yr < h; yr++) {
      size_t thread_count = h - (yr + 1);
      set_pixel_by_yr <<< thread_count, 1 >>> (devState, png_buf, w, yr);
    }


    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "blocks");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
}
