#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(curandState *devState, size_t random_dev_count, size_t seed, unsigned char *png_buf) {
  curand_init(seed, 0, 0, &devState[0]);
  for (size_t i = 1; i < random_dev_count; i++) {
    curand_init(curand_uniform(&*devState) * seed, 0, 0, &devState[i]);
  }
}


__global__ void set_color(curandState *devState, unsigned char *png_buf, size_t w) {
  size_t x = blockIdx.x % w;
  size_t y = blockIdx.x / w;
  png_buf[(((y * w) + x) * 3) + 0] = curand_uniform(&devState[(((y * w) + x))]) * 255.0;
  png_buf[(((y * w) + x) * 3) + 1] = curand_uniform(&devState[(((y * w) + x))]) * 255.0;
  png_buf[(((y * w) + x) * 3) + 2] = curand_uniform(&devState[(((y * w) + x))]) * 255.0;
}


__global__ void avg_color(curandState *devState, unsigned char *png_buf, unsigned char *png_buf2, size_t w, size_t h, int r) {
  int x = blockIdx.x % w;
  int y = blockIdx.x / w;
  
  int nx = 0;
  int ny = 0;
  int nx_2 = 0;
  int ny_2 = 0;
  int ws = 0;
  r += 1;
  for (int x2 = -r; x2 < r + 1; x2++) {
    if (x + x2 >= 0 && x + x2 < w) {
      for (int y2 = -r; y2 < r + 1; y2++) {
        if (y + y2 >= 0 && y + y2 < h) {
          int score = (
            abs(png_buf[((((y + y2) * w) + (x + x2)) * 3) + 0] - png_buf[((((y + y2) * w) + (x + x2)) * 3) + 1]) +
            abs(png_buf[((((y + y2) * w) + (x + x2)) * 3) + 0] - png_buf[((((y + y2) * w) + (x + x2)) * 3) + 2]) +
            abs(png_buf[((((y + y2) * w) + (x + x2)) * 3) + 1] - png_buf[((((y + y2) * w) + (x + x2)) * 3) + 2])
          ) / 3;
          if (score > ws) {
            ws = score;
            nx_2 = nx;
            ny_2 = ny;
            nx = x + x2;
            ny = y + y2;
          }
        }
      }
    }
  }
  for (char c = 0; c < 3; c++) {
    png_buf2[(((y * w) + x) * 3) + c] = (
      int(png_buf[(((y * w) + x) * 3) + c]) +
      int(png_buf[(((ny * w) + nx) * 3) + c]) + 
      int(png_buf[(((ny_2 * w) + nx_2) * 3) + c])
    ) / 3;
  }
}


int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}


int main() {

    size_t w = 1920*2;
    size_t h = 1080*2;
    size_t random_dev_count = w*h;

    curandState *devState;
    unsigned char *png_buf;
    unsigned char *png_buf2;

    cudaMalloc(&devState, sizeof(curandState) * random_dev_count);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);
    cudaMalloc(&png_buf2, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (devState, random_dev_count, time(0), png_buf);

    set_color <<< w * h, 1 >>> (devState, png_buf, w);
    for (int i = 0; i < 20; i++) {
      avg_color <<< w * h, 1 >>> (devState, png_buf, png_buf2, w, h, i);
      unsigned char *tmp = png_buf2;
      png_buf2 = png_buf;
      png_buf = tmp;
    }

    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "blocks");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
    cudaFree(png_buf2);
}
