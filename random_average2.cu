#include <stdlib.h>
#include <stdio.h>
#include <curand_kernel.h>
#include <png.h>

__global__ void setup(curandState *devState, size_t w, size_t seed) {
  curand_init(seed, 0, 0, &devState[0]);
  for (size_t i = 1; i < w; i++) {
    curand_init(curand_uniform(&devState[i-1]) * seed, 0, 0, &devState[i]);
  }
}

__global__ void set_pixel_by_yr(curandState *devState, unsigned char *png_buf, unsigned char *png_buf2, size_t w, size_t y) {
  size_t x = blockIdx.x;
  if (y > 0) {
    // Get average of 3 pixels above
    double tmp[] = {0, 0, 0};
    unsigned char count = 0;
    for (int x2 = -1 - (curand_uniform(&devState[x]) * 3); x2 < 2 + (curand_uniform(&devState[x]) * 2); x2++) {
      if (x + x2 < w && x + x2 >= 0) {
        count += 1;
        for (unsigned char c = 0; c < 3; c++) {
          tmp[c] += png_buf[((((y - 1) * w) + (x + x2)) * 3) + c] + png_buf2[((((y - 1) * w) + (x + x2)) * 3) + c];
        }
      }
    }
    // Adjust using average
    unsigned total = tmp[0] + tmp[1] + tmp[2];
    for (unsigned char c = 0; c < 3; c++) {
      tmp[c] /= count;
      png_buf[(((y * w) + x) * 3) + c] = tmp[c] * (total > 400 ? 0.99 : 1.01);
    }
  }
  if (curand_uniform(&devState[x]) < 0.005) {
    int i = (1 - curand_uniform(&devState[x])) * 3;
    png_buf2[(((y * w) + x) * 3) + i] = 255;
  }
}


int writeImage(char *filename, int width, int height, unsigned char *buffer, char *title) {
    int code = 0;
    // Open file for writing (binary mode)
    FILE *fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        code = 1;
        return code;
    }

    // Initialize write structure
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        code = 1;
        return code;
    }

    // Initialize info structure
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        code = 1;
        return code;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        code = 1;
        return code;
    }

    png_init_io(png_ptr, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    // Set title
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    png_bytep row = (png_bytep) malloc(width * sizeof(png_byte) * 3);

    // Write image data
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width * 3; x++) {
            row[x] = buffer[(y * width * 3) + x];
        }
        png_write_row(png_ptr, row);
    }

    // End write
    png_write_end(png_ptr, NULL);
    fclose(fp);
    return code;
}

int main() {

    size_t w = 1920*2;
    size_t h = 1080*2;

    curandState *devState;
    unsigned char *png_buf;
    unsigned char *png_buf2;

    cudaMalloc(&devState, sizeof(curandState) * w);
    cudaMalloc(&png_buf, sizeof(unsigned char) * w * h * 3);
    cudaMalloc(&png_buf2, sizeof(unsigned char) * w * h * 3);

    setup <<< 1, 1 >>> (devState, w, time(0));
    for (size_t y = 0; y < h; y++) {
      set_pixel_by_yr <<< w, 1 >>> (devState, png_buf, png_buf2, w, y);
    }


    unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * h * w * 3);
    cudaMemcpy(buffer, png_buf, sizeof(unsigned char) * h * w * 3, cudaMemcpyDeviceToHost);
    writeImage("out.png", w, h, buffer, "blocks");
    free(buffer);

    cudaFree(devState);
    cudaFree(png_buf);
    cudaFree(png_buf2);
}
